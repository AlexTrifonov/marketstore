import Model.CustomException;
import Model.DiscountCard;
import java.util.HashMap;

public class Admin {
    private HashMap<String, DiscountCard> cards = new HashMap();

    public void addCard(DiscountCard discountCard) {
        String owner = discountCard.getOwner();
        if (cards.containsKey(owner)) throw new CustomException("Card for " + owner + " already exists");
        cards.put(owner, discountCard);
    }

    public void deleteCard(String owner) {
        if(cards.containsKey(owner)) {
         cards.remove(owner);
        } else throw  new CustomException("No card for " + owner + " exists");
    }

    //I decided to store the cards and get them by name since I imagine in a real life scenario
    // they would be stored inside the system.
    public void printPurchase(String owner, double price) {

        DiscountCard card = cards.get(owner);

        try {
            double discount = card.getDiscount(price);
            System.out.println("Purchase value: " + price + "$");
            System.out.println("Discount rate: " + card.getDiscountRate() + "%");
            System.out.println("Discount " + discount + "$");
            System.out.println("Total " + (price - discount) + "$");
            System.out.println();
        } catch (NullPointerException ne) {
            throw new CustomException("No such card exists");
        }
    }
}
