import Model.BronzeCard;
import Model.GoldCard;
import Model.SilverCard;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    public void run() {
        Admin admin= new Admin();

        admin.addCard(new BronzeCard("John", 0));
        admin.printPurchase("John", 150);

        admin.addCard(new SilverCard("Liz", 600));
        admin.printPurchase("Liz", 850);

        admin.addCard(new GoldCard("Ann", 1500));
        admin.printPurchase("Ann", 1300);
    }
}
