package Model;

public class SilverCard extends DiscountCard {

    public SilverCard(String owner, double turnover) {
        super(owner, turnover);
    }

    @Override
    protected void calculateDiscountRate() {
        if (turnover <= 300) {
            discountRate = 2.0;
        } else discountRate = 3.5;
    }
}
