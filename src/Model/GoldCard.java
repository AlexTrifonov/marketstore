package Model;

public class GoldCard extends DiscountCard{

    public GoldCard(String owner, double turnover) {
        super(owner, turnover);
    }

    @Override
    protected void calculateDiscountRate() {
        discountRate = 2.0;

        for (int i = 0; i < (int) turnover/100 && i < 8; i++) {
            discountRate++;
        }
    }
}
