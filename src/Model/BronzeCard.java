package Model;

public class BronzeCard extends DiscountCard{

    public BronzeCard(String owner, double turnover) {
        super(owner, turnover);
    }

    @Override
    protected void calculateDiscountRate() {
        if (turnover > 300) {
            discountRate = 2.5;
        } else if (turnover <= 300 && turnover >= 100) {
            discountRate = 1.0;
        } else discountRate = 0;
    }
}
