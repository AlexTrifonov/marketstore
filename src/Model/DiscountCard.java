package Model;

public abstract class DiscountCard {
    protected String owner;
    protected double discountRate;
    protected double turnover;

    public DiscountCard(String owner, double turnover) {
        this.owner = owner;
        this.turnover = turnover;
        calculateDiscountRate();
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
        calculateDiscountRate();
    }

    protected abstract void calculateDiscountRate();

    public String getOwner() {
        return owner;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public double getTurnover() {
        return turnover;
    }

    public double getDiscount(double price) {
        return price*discountRate/100;
    }
}
